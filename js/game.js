
let colors = ["crimson", "lightseagreen", "cornflowerblue", "chartreuse", "palegoldenrod", "chocolate", "blueviolet", "fuchsia"];

let solution = ["empty", "empty", "empty", "empty", "empty"]; //["blueviolet", "chartreuse", "fuchsia", "lightseagreen", "chocolate"];

//funkcja losująca zawartość tablicy solution
function randomSolution() {
//     for(let i of solution) {
//         let test = parseInt(Math.random() * (8 - 0) + 0);
//         console.log("Zawartosc indeksu przed zmiana: ", i)
//         i = colors[test];
//         console.log("Wylosowany nr: ", test, ", zawartosc indeksu tablicy: ", i);
//     }
    for(i = 0; i < solution.length; i++) {
        let test = parseInt(Math.random() * (8 - 0) + 0);
        //console.log("Zawartosc indeksu przed zmiana: ", solution[i]);
        solution[i] = colors[test];
        //console.log("Wylosowany nr: ", test, ", zawartosc indeksu tablicy: ", solution[i]);
    }
    //console.log("Wylosowane kolory: ", solution);
}

let state = ["empty", "empty", "empty", "empty", "empty"];

const canvas = document.getElementById("game_canvas");
const ctx = canvas.getContext("2d");

const PILL_WIDTH = 80;
const PILL_HEIGHT = 80;

let active_pill = 0;
let round_no = 1; //0;

//zmienne trzymające linki do obrazków
const ok = new Image(50, 50);
ok.src = "assets/ok.png";
const elsewhere = new Image(50, 50);
elsewhere.src = "assets/elsewhere.png";
const none = new Image(50, 50);
none.src = "assets/none.png";

let temp; //zmienna do ustawiania różnych rzeczy, po to aby nie tworzyć mnóstwa zmiennych w programie

for(i = 0; i < 8; i++) {
    temp = document.getElementById("c"+(i+1)); //nie może być querySelector, gdyż wywala błąd "Type Error: temp is null"
    //let temp2 = document.querySelector("#c"+(i+1)); //ale takie rozwiązanie już działa. Podejrzewam że po prostu zmienna musi zostać zdefiniowana w momencie użycia querySelector, dlaczego? Nie wiem.
    //let temp2 = i; //to działa i pokazuje kolor, a nie i=8 jak wcześniej
    temp.style.backgroundColor = colors[i];
    //temp.innerHTML = colors[i];
    temp.addEventListener('click', guess, false);
    temp.kolorek = colors[i]; //przekazanie koloru jako parametr z informacją jaki kolor kliknięto (przekazanie koloru jako właściwość do funkcji). Jest to niestandardowa właściwość JS, tylko utworzona przez programistę!!! Nie oznacza ona koloru czcionki!!! Zmieniłam nazwę z color na kolorek, żeby to lepiej widzieć
}

function guess(evt) {
    //console.log(evt.currentTarget.kolorek);

    if(active_pill >= 0 && active_pill <= 4) {
        temp = evt.currentTarget.kolorek;
        drawPill(100 + active_pill * 100, 50, temp);
        state[active_pill] = temp;

        if(active_pill < 4)
            active_pill++;

        else
            checkBoard();

        drawArrow();
    }
}

function checkBoard() {
    console.log("Stan planszy: " + state);

    //to czyści planszę po wybraniu koloru dla 5-go klocka
    active_pill = 0;
    startBoard();
    drawArrow();

    for(i = 0; i < 5; i++) {
        //to rysuje wybrane kolorki poniżej, po wyczyszczeniu planszy
        drawPill(100 + i * 100, 250, state[i]);
    }

    //to rysuje czerwony prostokąt w miejscu gdzie będą rysowane emotikonki wskazujące czy się trafiło kolor czy nie
    ctx.fillStyle = "black";
    ctx.fillRect(100, 350, 480, 80);

    for(i = 0; i < 5; i++) {
        if(state[i] == solution[i])
            ctx.drawImage(ok, 115 + i * 100, 360);

        else {
            let colorExist = false; //zmienna typu bool wskazująca iż dany kolor wystąpił, ale na innej pozycji

            for(j = 0; j < 5; j++) {
                if(state[i] == solution[j])
                    colorExist = true
            }

            if(colorExist)
                ctx.drawImage(elsewhere, 115 + i * 100, 360);
            else
                ctx.drawImage(none, 115 + i * 100, 360);
        }
    }

    //sprawdzenie czy gracz odgadł wszystkie kolory
    let win = true;

    for(i = 0; i < 5; i++) {
        if(state[i] != solution[i])
            win = false;
    }

    if(win) {
        //zakrycie kwadracików do odgadywania kolorów bo gracz wygrał
        ctx.fillStyle = "black";
        ctx.fillRect(80, 20, 550, 200);

        active_pill = -666; //wywalenie strzałki poza okno gry
        drawArrow();

        ctx.fillStyle = "orange";
        ctx.font = "78px Arial";
        ctx.fillText("WYGRAŁEŚ!", 160, 130);
        document.querySelector(".comment").innerHTML = "Udało Ci się ustalić kolory wszystkich bloków w rundzie nr " + round_no;

        //ukrycie przycisków z kolorkami do wyboru
        for(i = 0; i < 8; i++) {
            let name = "#c" + (i + 1);
            let elem = document.querySelector(name);
            elem.style.display = "none";
        }

        //ukrycie przycisku usuwania koloru z bloczka
        document.querySelector("#delete").style.display = "none";

        document.querySelector(".controls").style.fontSize = "40px";
        document.querySelector(".controls").innerHTML = '<span id="again">Zagramy jeszcze raz?</span>';
        document.querySelector("#again").style.cursor = "pointer";
        //document.querySelector("#again:hover").style.color = "orange";
        document.querySelector("#again").addEventListener("click", function(){
            window.location.reload();
        }, false);
    }

    else {
        round_no++;
        drawScore();
    }
}

function drawPill(x, y, type) {
    //domyślne zamalowanie nieco większego fragmentu ekranu gdzie są rysowane sloty na kolory na czarno, żeby "wymazać" stary kolor w przypadku usunięcia koloru z poprzedniego slotu i próby ustawienia nowego koloru - inaczej nie zechce narysować innego koloru na ekranie, mimo że wartość tablicy z wybranymi kolorami się zmieni
    ctx.fillStyle = "black";
    ctx.fillRect(x-5, y-5, PILL_WIDTH+10, PILL_HEIGHT+10);

    if(type == "empty") {
        ctx.strokeStyle = "white";
        ctx.strokeRect(x, y, PILL_WIDTH, PILL_HEIGHT);
    }
    else {
        ctx.strokeStyle = type;
        ctx.strokeRect(x, y, PILL_WIDTH, PILL_HEIGHT);
        ctx.fillStyle = type;
        ctx.fillRect(x, y, PILL_WIDTH, PILL_HEIGHT);
    }
}

function drawArrow() {
    //document.write("&uarr;");
    ctx.fillStyle = "black";
    ctx.fillRect(100, 150, 480, 45);

    ctx.fillStyle = "white";
    ctx.font = "32px Arial";
    temp = decodeHTMLEntities("&uarr;");
    ctx.fillText(temp, 135 + active_pill * 100, 180);
}

function startBoard() {
//     randomSolution();
    ctx.fillStyle = "black";
    ctx.fillRect(90, 40, 500, 100);

    for(i = 0; i < 5; i++) {
        drawPill(100 + i * 100, 50, "empty");
    }

    drawArrow();
}

//funkcja do usuwania ustawionego kolorku kwadracika, niestety ostatniego kwadracika nie da się zmienić póki co
function resetPill() {
    if(active_pill > 0) {
        drawPill(100 + (active_pill - 1) * 100, 50, "empty");
        state[active_pill - 1] = "empty";
        console.log("resetPill: " + state);
        active_pill--;
        drawArrow();
    }
}

function drawScore() {
    ctx.fillStyle = "black";
    ctx.fillRect(640, 40, 110, 130);

    //rysowanie kółka
    ctx.strokeStyle = "orange";
    ctx.beginPath();
    ctx.arc(692, 90, 45, 0, 2 * Math.PI);
    ctx.stroke();

    ctx.fillStyle = "orange";
    ctx.font = "44px Arial";

    if(round_no < 10)
        ctx.fillText(round_no, 680, 105);

    else
        ctx.fillText(round_no, 670, 105);

    ctx.font = "20px Arial";
    ctx.fillText("RUNDA", 660, 165);
}

window.addEventListener('load', function() {
    randomSolution();
    startBoard();
    drawScore();
});

//funkcja zamieniająca encje HTML na znaki, gdyż JS nie potrafi konwertować encji HTML na znaki

function decodeHTMLEntities(text) {
    var textArea = document.createElement('textarea');
    textArea.innerHTML = text;
    //console.log(textArea.value);
    return textArea.value;
}

function showHelp() {
    document.querySelector(".help").style.display = "block";
}

function closeHelp() {
    document.querySelector(".help").style.display = "none";
}

document.querySelector("#delete").addEventListener("click", resetPill, false);
document.querySelector("#help").addEventListener("click", showHelp, false);
document.querySelector("#closehelp").addEventListener("click", closeHelp, false);
